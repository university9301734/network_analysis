%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Second and fifth points of HW 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

addpath(genpath('utils'))

n = 48;
rep = 1000;

%% Uniform(0,1) Case
real_U_mean = 0.5;
results_U_A = zeros(rep, 3);
%results_U_T = zeros(rep, 3);

for i = 1:rep
    X = rand(1, n);
    
    results_U_A(i, 1) = emp_mean(X);
    results_U_A(i, 2:end) = AsymptoticCI(X, 0.95);
    
%     results_U_T(i, 1) = emp_mean(X);
%     results_U_T(i, 2:end) = StudentTCI(X, 0.95);
end

[~, sort_perm] = sort(results_U_A(:, 2));
results_U_A = results_U_A(sort_perm, :);

%[~, sort_perm] = sort(results_U_T(:, 2));
%results_U_T = results_U_T(sort_perm, :);

count_U_A = 0;
%count_U_T = 0;
for i = 1:rep
    if results_U_A(i, 2) > real_U_mean || results_U_A(i, 3) < real_U_mean
        count_U_A = count_U_A + 1;
    end
    
%     if results_U_T(i, 2) > real_U_mean || results_U_T(i, 3) < real_U_mean
%         count_U_T = count_U_T + 1;
%     end
end

figure;
% subplot(1,2,1)
hold on
grid on
h0 = plot(results_U_A(:, 1), 'b.');
h1 = plot(results_U_A(:, 2), 'g--');
plot(results_U_A(:, 3), 'g--');
h2 = plot([0,1000], [real_U_mean, real_U_mean], 'k--');
title('Uniform Case, Asymptotic')
legend([h0, h1, h2], 'Emp. mean', 'CI', 'Real mean', 'Location', 'northwest')
hold off

% subplot(1,2,2)
% hold on
% grid on
% h0 = plot(results_U_T(:, 1), 'b.');
% h1 = plot(results_U_T(:, 2), 'g-');
% plot(results_U_T(:, 3), 'g-');
% h2 = plot([0,1000], [real_U_mean, real_U_mean], 'k--');
% title('Uniform Case, Student''s T')
% legend([h0, h1, h2], 'Emp. mean', 'CI', 'Real mean', 'Location', 'northwest')
% hold off

%% Normal(0,1) Case
real_N_mean = 0;
results_N_A = zeros(rep, 3);
results_N_T = zeros(rep, 3);

for i = 1:rep
    X = randn(1, n);
    
    results_N_A(i, 1) = mean(X);
    results_N_A(i, 2:end) = AsymptoticCI(X, 0.95);
    
    results_N_T(i, 1) = mean(X);
    results_N_T(i, 2:end) = StudentTCI(X, 0.95);
end

[~, sort_perm] = sort(results_N_A(:, 2));
results_N_A = results_N_A(sort_perm, :);

[~, sort_perm] = sort(results_N_T(:, 2));
results_N_T = results_N_T(sort_perm, :);

count_N_A = 0;
count_N_T = 0;
for i = 1:rep
    if results_N_A(i, 2) > real_N_mean || results_N_A(i, 3) < real_N_mean
        count_N_A = count_N_A + 1;
    end
    
    if results_N_T(i, 2) > real_N_mean || results_N_T(i, 3) < real_N_mean
        count_N_T = count_N_T + 1;
    end
end

figure;
subplot(1,2,1)
hold on
grid on
h0 = plot(results_N_A(:, 1), 'b.'); 
h1 = plot(results_N_A(:, 2), 'g--');
plot(results_N_A(:, 3), 'g--');
h2 = plot([0,1000], [real_N_mean, real_N_mean], 'k--');
title('Normal Case, Asymptotic')
legend([h0, h1, h2], 'Emp. mean', 'CI', 'Real mean', 'Location', 'northwest')
hold off

subplot(1,2,2)
hold on
grid on
h0 = plot(results_N_T(:, 1), 'b.');
h1 = plot(results_N_T(:, 3), 'g--');
plot(results_N_T(:, 2), 'g--');
h2 = plot([0,1000], [real_N_mean, real_N_mean], 'k--');
title('Normal Case, Student''s T')
legend([h0, h1, h2], 'Emp. mean', 'CI', 'Real mean', 'Location', 'northwest')
hold off

fprintf(strcat('Out of 1000 repetitions, exactly',...
        '\n\t%d times for the Uniform case with Asymptotic CI', ...
        '\n\t%d times for the Normal case with Asymptotic CI', ...
        '\n\t%d times for the Normal case with Student''s T CI', ...
        '\nthe real mean was not included in the relative CI\n'), ...
        count_U_A, count_N_A, count_N_T);
