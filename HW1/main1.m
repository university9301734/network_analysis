%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% First point of HW 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

addpath(genpath('utils'))

old = load('DATA//figure 2.1//sgbdold.dat');
new = load('DATA//figure 2.1//sgbdnew.dat');

%% Figure 2.1, 2.2, 2.3
old = sort(old);
new = sort(new);

[Fo, Xo, yo, xo] = emp_cdf(old);
[Fn, Xn, yn, xn] = emp_cdf(new);
median_o = (old(50)+old(51))/2;
median_n = (new(50)+new(51))/2;

i = abs(Fo-0.75) < 1e-9; % more efficient than find(...) according to MATLAB suggestions
quantile75_o = Xo(i);
i = abs(Fn-0.75) < 1e-9;
quantile75_n = Xn(i);

i = abs(Fo-0.25) < 1e-9;
quantile25_o = Xo(i);
i = abs(Fn-0.25) < 1e-9;
quantile25_n = Xn(i);

mean_o = emp_mean(old);
s_o = emp_var(old);
mean_n = emp_mean(new);
s_n = emp_var(new);

CImedian_o = pQuantileCI(old, 0.5, 0.95);
CImedian_n = pQuantileCI(new, 0.5, 0.95);

CImean_o = StudentTCI(old, 0.95);
CImean_n = StudentTCI(new, 0.95);

PI_o = max(0, PredIntervals(old, 0.95, true)); % negative values not allowed
PI_n = max(0, PredIntervals(new, 0.95, true));


figure;
subplot(2,2,1)
histogram(old, 10, 'Normalization', 'pdf');
title('FIG 2.1, .2, .3: Old pdf');

subplot(2,2,2)
histogram(new, 10, 'Normalization', 'pdf');
title('New pdf');

subplot(2,2,3)
hold on
plot(xo,yo);
plot(xn,yn);
legend('old', 'new', 'Location', 'southeast')
title('Old vs New systems ECDF')
xlabel('x');
ylabel('F(x)')

subplot(2,2,4)
grid on
hold on
h1 = errorbar(0.8, median_o, (CImedian_o(2) - CImedian_o(1))/2, 'bx');
errorbar(2.8, median_n, (CImedian_n(2) - CImedian_n(1))/2, 'bx');

h2 = errorbar(1.2, mean_o, (CImean_o(2) - CImean_o(1))/2, 'ko');
errorbar(3.2, mean_n, (CImean_n(2)- CImean_n(1))/2, 'ko');

h3 = errorbar(1, 0, PI_o(1), PI_o(2), 0 , 0, 'r');
errorbar(3, 0, PI_n(1), PI_n(2), 0 , 0, 'r');

h4 = plot([0.5, 1.5], [quantile25_o, quantile25_o], 'g');
plot([0.5, 1.5], [quantile75_o, quantile75_o], 'g');
plot([2.5, 3.5], [quantile25_n, quantile25_n], 'g');
plot([2.5, 3.5], [quantile75_n, quantile75_n], 'g');
title('Some CI')
xlabel('Old = left cluster, New = right cluster')
legend([h1,h2,h3,h4], 'Median', 'Mean', 'PI', 'Quantile');


%% Figure 2.7
old2 = load('DATA//figure 2.1//sgbdold.dat');
new2 = load('DATA//figure 2.1//sgbdnew.dat');

diff = old2 - new2;
figure;
subplot(1,3,1)
plot(diff, '+');
title('FIG 2.7: Difference')

diff = sort(diff);
clear old2
clear new2

[Fd, Xd] = emp_cdf(diff);
mean_d = emp_mean(diff);
median_d = (diff(50)+diff(51))/2;
i = abs(Fd-0.75) < 1e-9;
quantile75_d = Xd(i);
i = abs(Fd-0.25) < 1e-9;
quantile25_d = Xd(i);

CImedian_d = pQuantileCI(diff, 0.5, 0.95);

CImean_d = StudentTCI(diff, 0.95);

PI_d = PredIntervals(diff, 0.95, true);



subplot(1,3,2)
histogram(diff, 10, 'Normalization', 'pdf')
title('Difference pdf')

subplot(1,3,3)
grid on
hold on
h1 = errorbar(0.8, median_d, (CImedian_d(2)- CImedian_d(1))/2, 'bx');

h2 = errorbar(1.2, mean_d, (CImean_d(2) - CImean_d(1))/2, 'ko');

h3 = errorbar(1, 0, PI_d(1), PI_d(2), 0 , 0, 'r');

h4 = plot([0.5, 1.5], [quantile25_d, quantile25_d], 'g');
plot([0.5, 1.5], [quantile75_d, quantile75_d], 'g');
title('Some CI')
legend([h1,h2,h3,h4], 'Median', 'Mean', 'PI', 'Quantile');



%% Figure 2.8
CImean_o_N = AsymptoticCI(old, 0.95);
CImean_n_N = AsymptoticCI(new, 0.95);

CImean_o_B = bootstrapCI(old, 0.95, 25, @emp_mean);
CImean_n_B = bootstrapCI(new, 0.95, 25, @emp_mean);

figure;
hold on
grid on
h1 = errorbar(1, mean_o, (CImean_o(2) - CImean_o(1))/2, 'k*');
errorbar(3, mean_n, (CImean_n(2) - CImean_n(1))/2, 'k*');

h2 = errorbar(1.2, mean_o, (CImean_o_N(2) - CImean_o_N(1))/2, 'r*');
errorbar(3.2, mean_n, (CImean_n_N(2) - CImean_n_N(1))/2, 'r*');

h3 = errorbar(1.4, mean_o, (CImean_o_B(2) - CImean_o_B(1))/2, 'b*');
errorbar(3.4, mean_n, (CImean_n_B(2) - CImean_n_B(1))/2, 'b*');

plot(0.9, 0); %to pan the visual

xlabel('Old = left cluster, New = right cluster')
title('FIG 2.8')
legend([h1,h2,h3], 'Normal', 'Asymptotic', 'Bootstrap')

%% Figure 2.10
joe = load('DATA//figure 2.10//joe.dat');
figure;
plot(joe, 'r')
title('FIG 2.10: Joe''s original dataset')


A = autocorr(joe, length(joe)-1);
bound = 1.96 * 1/length(joe)^0.5;

figure;
hold on
bar(1:length(A), A, 0.1);
plot([0, length(A)], [bound, bound], 'b--');
plot([0, length(A)], [-bound, -bound], 'b--');
title('FIG 2.10: Joe''s autocorrelation')

figure;
for i = 1:9
    subplot(3,3,i);
    x = joe(1:end-i);
    y = joe(i+1:end);
    plot(x,y, '.');
    title(sprintf('Lag plot, h = %d', i));
end




