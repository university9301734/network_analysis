%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Fourth and fifth points of HW 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;

addpath(genpath('utils'));

max_steps = 500;
step = 2; % then max n = max_steps * step
r = 100; % repetitions for the MSE estimate
    
%% Uniform case
disp('Uniform case')
mean_U = zeros(max_steps, 1);
var_U = zeros(max_steps, 1);
varCI_U = zeros(max_steps, 2);
mean_MSE_U = zeros(max_steps, 1);
var_MSE_U = zeros(max_steps, 1);

real_U_mean = 0.5;
real_U_var = 1/12;
parfor n = 1:max_steps
    n1 = n*step;
    fprintf('%d\n', n1);
    X = rand(1, n1);
    mean_U(n) = emp_mean(X);
    var_U(n) = emp_var(X);
    
    mean_MSE_U(n) = emp_mean( (bootstrp(r, @emp_mean, X) - real_U_mean).^2 );
    var_MSE_U(n) = emp_mean( (bootstrp(r, @emp_var, X) - real_U_var).^2 );
        
    varCI_U(n, :) = bootstrapCI(X, 0.95, 25, @emp_var);  
end

% bsPI_var_U = zeros(max_steps, 2);
% for n = 1:max_steps
%     Y = var_U(1:n);
%     if n < 39        
%         bsPI_var_U(n, :) = [min(Y), max(Y)];  % WARNING: this PI is less than 95% accurate 
%     else
%         Y = sort(Y);
%         a2 = 0.025;
%         bsPI_var_U(n, :) = [Y(floor((n+1) * a2)); Y(ceil((n+1) * (1-a2)))];
%     end
% end

Y = sort(var_U);
a2 = 0.025;
n = length(var_U);
bsPI_var_U = zeros(n, 2);
bsPI_var_U(:, 1) = bsPI_var_U(:, 1) + Y(floor((n+1) * a2));
bsPI_var_U(:, 2) = bsPI_var_U(:, 2) + Y(ceil((n+1) * (1-a2)));

tries = step:step:max_steps*step;
 
figure;
subplot(2,2,1)
hold on
grid on
plot(tries, mean_U, '.');
h = plot([min(tries), max(tries)], [real_U_mean, real_U_mean]);
legend(h, 'real mean');
title('Uniform case, mean')
xlabel('n')

subplot(2,2,2)
hold on
grid on
% plot(tries, var_U, '.');
h(1) = errorbar(tries, var_U, (varCI_U(:, 2)-varCI_U(:, 1))/2, '.');
h(2) = plot([min(tries), max(tries)], [real_U_var, real_U_var]);
tmp1 = bsPI_var_U(:, 1);
tmp2 = bsPI_var_U(:, 2);
h(3) = plot(tries, tmp1, 'r');
plot(tries, tmp2, 'r');
legend(h, 'emp. var. + CI', 'real variance', 'pred. intervals')
title('Uniform case, variance')
xlabel('n')

subplot(2,2,3)
plot(tries, mean_MSE_U);
title('MSE(F_e) of mean')
xlabel('n')

subplot(2,2,4)
plot(tries, var_MSE_U);
title('MSE(F_e) of variance')
xlabel('n')

%% Normal Case
disp('Normal case')
mean_N = zeros(max_steps, 1);
var_N = zeros(max_steps, 1);
varCI_N = zeros(max_steps, 2);
mean_MSE_N = zeros(max_steps, 1);
var_MSE_N = zeros(max_steps, 1);


real_N_mean = 0;
real_N_var = 1;
idx = 1;
parfor n = 1:max_steps
    n1 = n*step;
    fprintf('%d\n', n1);
    X = randn(1, n1);
    mean_N(n) = emp_mean(X);
    var_N(n) = emp_var(X);
    
    mean_MSE_N(n) = emp_mean( (bootstrp(r, @emp_mean, X) - real_N_mean).^2 );
    var_MSE_N(n) = emp_mean( (bootstrp(r, @emp_var, X) - real_N_var).^2 );
    
    varCI_N(n, :) = bootstrapCI(X, 0.95, 25, @emp_var);
    varCI_N_exact(n, :) = [var_N(n) * (n1-1) / chi2inv(0.025, n1-1), var_N(n) * (n1-1) / chi2inv(0.975, n1-1)];
end

% bsPI_var_N = zeros(max_steps, 2);
% for n = 1:max_steps
%     Y = var_N(1:n);
%     if n < 39        
%         bsPI_var_N(n, :) = [min(Y), max(Y)];  % WARNING: this PI is less than 95% accurate 
%     else
%         Y = sort(Y);
%         a2 = 0.025;
%         bsPI_var_N(n, :) = [Y(floor((n+1) * a2)); Y(ceil((n+1) * (1-a2)))];
%     end
% end

Y = sort(var_N);
a2 = 0.025;
n = length(var_N);
bsPI_var_N = zeros(n, 2); %Bootstrap
bsPI_var_N(:, 1) = bsPI_var_N(:, 1) + Y(floor((n+1) * a2));
bsPI_var_N(:, 2) = bsPI_var_N(:, 2) + Y(ceil((n+1) * (1-a2)));

% AsPI_var_N = zeros(n, 2); %Asymptotic
% m = emp_mean(var_N);
% s = (emp_var(var_N))^0.5;
% AsPI_var_N(:, 1) = AsPI_var_N(:, 1) + m - norminv(0.975) * s;
% AsPI_var_N(:, 2) = AsPI_var_N(:, 2) + m + norminv(0.975) * s;

figure;
subplot(2,2,1)
hold on
grid on
plot(tries, mean_N, '.');
h = plot([min(tries), max(tries)], [real_N_mean, real_N_mean]);
legend(h, 'real mean')
title('Normal case, mean')
xlabel('n')

subplot(2,2,2)
hold on
grid on
% plot(tries, var_N, '.');
h(1) = errorbar(tries, var_N, (varCI_N(:, 2)-varCI_N(:, 1))/2, '.');
h(2) = plot([min(tries), max(tries)], [real_N_var, real_N_var]);
tmp1 = bsPI_var_N(:, 1);
tmp2 = bsPI_var_N(:, 2);
h(3) = plot(tries, tmp1, 'r');
plot(tries, tmp2, 'r');
% tmp1 = AsPI_var_N(:, 1);
% tmp2 = AsPI_var_N(:, 2);
% h(4) = plot(tries, tmp1, 'k');
% plot(tries, tmp2, 'k');

legend(h, 'emp. var. + CI', 'real variance', 'pred. interval - bootstrap') %, 'pred. interval - asymptotic')
title('Normal case, variance')
xlabel('n')

subplot(2,2,3)
plot(tries, mean_MSE_N);
title('MSE(F_e) of mean')
xlabel('n')

subplot(2,2,4)
plot(tries, var_MSE_N);
title('MSE(F_e) of variance')
xlabel('n')

figure;
hold on
grid on
varCI_N_exact(1, :) = [0,0];
h(1) = errorbar(tries, var_N, (varCI_N_exact(:, 2)-varCI_N_exact(:, 1))/2, '.');
h(2) = plot([min(tries), max(tries)], [real_N_var, real_N_var]);
tmp1 = bsPI_var_N(:, 1);
tmp2 = bsPI_var_N(:, 2);
h(3) = plot(tries, tmp1, 'r');
plot(tries, tmp2, 'r');
legend(h, 'emp. var. + CI', 'real variance', 'pred. interval')
title('Normal case, variance with exact CI (Normal assumption)')
xlabel('n')

