%% Find empirical mean of data
function [mean] = emp_mean(data)
    n = length(data);
    mean = sum(data);
    mean = mean / n;
end