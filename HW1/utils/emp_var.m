%% Find empirical variance of data, either biased or unbiased. Default is unbiased.
function [var] = emp_var(data, varargin)
    biased = false;
    if nargin > 1
        biased = varargin{1};
    end
    
    n = length(data);
    var = sum((emp_mean(data) - data).^2);
    
    if biased
        var = var/n;
    else
        var = var/(n-1);
    end
end