function [CI] = bootstrapCI(X, g, r0, func)
    R = ceil(2 * r0/(1-g)) - 1;
    T = zeros(1,R);
    n = length(X);
    
    for r = 1:R
        %draw with replacement
        Xr = zeros(1,n);
        for j = 1:n
            i = randi(n); 
            Xr(j) = X(i);
        end

        T(r) = func(Xr);
    end

    T = sort(T);
    CI = [T(r0), T(R+1-r0)];
end