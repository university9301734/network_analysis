%% Find prediction intervals of level 0<a<1 for the data.
function [PI] = PredIntervals(data, a, are_data_normal_distributed)
    assert(a>0 && a<1, 'The parameter a must be in (0,1) interval.');
    n = length(data);
    
    if ~are_data_normal_distributed
        data = sort(data);        
        if n < 39
            PI = [data(1), data(end)];
            return;
        end
        
        g = 1-a;
        j = (n+1) * g/2;
        k = (n+1) * (1 - g/2);
        PI = [data(j), data(k)];        
    else
        m = emp_mean(data);
        s = (emp_var(data)) ^ 0.5;
        if n < 50 %pag 46, Le Boudec, 'Performance analysis'
            nu = tinv((1+a)/2, n-1);
            PI = [m - nu * (1 + 1/n)^0.5 * s, m + nu * (1 + 1/n)^0.5 * s];
        else
            nu = norminv((1+a)/2);            
            PI = [m - nu * s, m + nu * s];
        end
    end
end