%% Find the empirical CDF of some data
function [F, X, toplot_F, toplot_X] = emp_cdf(data)
    singletons = unique(data);
    singletons = sort(singletons);
    n = length(singletons);
    F = zeros(n + 1, 1);
    X = zeros(n + 1, 1);
    
    X(2:end) = singletons;
    X(1) = singletons(1);
    
    for i = 1:n
        tmp = i-1;
        for j = i:n
            if singletons(j) <= singletons(i)
                tmp = tmp + 1;
            end
        end
        
        F(i+1) = tmp/n;
    end
    
    toplot_F = zeros(length(F) * 2, 1);
    toplot_X = zeros(length(X) * 2, 1);
    toplot_F(1:2:end) = F-1/n;
    toplot_F(2:2:end-1) = F(1:end-1);
    toplot_F(end) = 1;
    toplot_F(1) = 0;
    toplot_X(1:2:end) = X;
    toplot_X(2:2:end) = X;
end