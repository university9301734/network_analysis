%% Find Confidence intervals in the Asymptotic case
function [CImean] = AsymptoticCI(X, a)
    assert(a > 0 && a < 1, 'It must be 0 < a < 1');    
    n = length(X);
    s = ( emp_var(X) )^0.5;
    n = n^0.5;
    m = emp_mean(X);
    
    nu = norminv((1+a)/2);
    CImean = [m - nu * s / n, m + nu * s / n];
end