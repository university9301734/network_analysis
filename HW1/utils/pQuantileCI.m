%% Find the Ci for the 0<p<1 quantile, with confidence 0<a<1.
function [CI] = pQuantileCI(data, p, a)
    assert(p>0 && p<1, 'The parameter p must be in (0,1) interval.');
    assert(a>0 && a<1, 'The parameter a must be in (0,1) interval.');
    
    n = length(data);
    data = sort(data);
    
    approx = n > 71; % as per slides, approx to normal distribution is n big enougth
    if ~approx
        j = fix(n/2);
        k = fix(n/2) + 1;
        while binocdf(k-1, n, p) - binocdf(j-1, n, p) < a
            j = j-1;
            k = k+1;
            if j < 0 || k > n
                warning('CI does not exist.');
                CI = [0,0];
                return;
            end
        end
        CI = [data(j), data(k)];
    else
        nu = norminv((1+a)/2);
        j = floor(n*p - nu*(n*p*(1-p))^0.5);
        k = ceil(n*p + nu*(n*p*(1-p))^0.5) + 1;
        
        CI = [data(j), data(k)];
    end
end