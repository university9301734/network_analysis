%% Find Confidence Intervals when data are know to come from a normal distribution, using Student T quantile
function [CImean] = StudentTCI(X, a)
    assert(a < 1, 'It must be 0 < a < 1');
    n = length(X);
    m = emp_mean(X);
    s = emp_var(X);
    
    nu = tinv((1+a)/2, n-1);    
    CImean = [m - nu * (s/n)^0.5, m + nu * (s/n)^0.5];
end