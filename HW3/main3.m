%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ES 3 of HW 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
addpath(genpath('utils'))

rho = 20;
p_awake = 0.05;
M = rho * p_awake * pi; % average number of relays per unit (circle) area
D = [5, 10, 20]; %distance from destination

v = 40; %quantization step
rep = 1e5;
all_M = 1:30;
all_results = cell(length(D), 2);

%% Monte Carlo, Figure 1,2,3  
tic
for i = 1:length(D)
    d = D(i);   
    result = zeros(length(all_M), 3);
    Ds = gpuArray(zeros(rep, 1) + d);
    
    for M = all_M
        Ms = gpuArray(zeros(rep, 1) + M);  
        ns = arrayfun(@geraf_monte_carlo, Ms, Ds); 
        result(M, :) = [M, gather(mean(ns)), gather(std(ns))];
    end 
    
    all_results{i, 1} = result;
end
toc

%% Analitical bounds, Figure 8,9,10
tic
for i = 1:length(D)
    d = D(i);                
    result = zeros(length(all_M), 3);
    for M = all_M
        res1 = geraf_upper_bound(M, d, v);
        res2 = geraf_lower_bound(M, d, v);
        result(M, :) = [M, res2, res1];       
    end
    
    all_results{i, 2} = result;
        
end
toc

for i = 1:length(D)
    d = D(i);
        
    result = all_results{i, 1};
    
    figure;
    grid on
    ax = errorbar(result(:, 1), result(:, 2), result(:, 3), '^');
    title(sprintf('D = %d', d));
    xlabel('average number of nodes in range')
    ylabel('number of hops (avg +/- std)')
%     legend(ax, 'Monte Carlo sim')
    
    result = all_results{i, 2};
    
    %figure;
    grid on
    hold on
    result = abs(result);   
    ax1 = plot(result(:, 1), result(:, 2), '--');
    ax2 = plot(result(:, 1), result(:, 3));
    hold off
    title(sprintf('D = %d', d));
    legend([ax1, ax2], 'lower bound', 'upper bound')
end

























%% Figure 8,9,10
% BETA = @(x,y) 0.5 * (pi - 2 * asin(x./y) - 2 * x./y .* (1-(x./y).^2).^0.5);
% A = @(r, D) BETA((D.^2 - r.^2 + 1)./(2*D), 1) + BETA(D - (D.^2 - r.^2 + 1)./(2*D), r);
% w = @(ik, v, M) ( exp(- M * A((ik(:, 1)-v+ik(:, 2)-1)/v, ik(:, 1)/v) / pi) - exp(- M * A((ik(:, 1)-v+ik(:, 2))/v, ik(:, 1)/v) / pi) ) .* double(ik(:, 2) > 0 & ik(:, 2) <= v);
% w0 = @(i, v, M) exp(- M * A(i/v, i/v) / pi);

% n1 = @(i, v, n1) double(i > v) .* (1 + sum(w([zeros(v-1, 1) + i, (1:v-1)'], v) .* n1(i - v + (1:v-1)', v, n1) )) / (1 - w0(i, v) - w([i,v], v)) + double(i <= v);
% n2 = @(i, v, n2) double(i > v) .* (1 + sum(w([zeros(v, 1) + i, (1:v)'], v) .* n2(i - v + (1:v)' - 1, v, n2) )) / (1 - w0(i, v)) + double(i <= v);

% for d = D
%     result = zeros(length(rhos), 3);
%     for j = 1:length(rhos)
%         M = rhos(j) * p_awake * pi;
%         state1 = zeros(d * v, 1);
%         state2 = zeros(d * v, 1);
%         for i = 1:d*v
%             %upper bound
%             if i > v        
%                 %state1(i) = (1 + sum(w([zeros(v-1, 1) + i, (1:v-1)'], v) .* state1(i - v + (1:v-1)', v, M) )) ./ (1 - w0(i, v) - w([i, zeros(length(i), 1)+v], v));        
%                 s = 0;
%                 for k = 1:v-1
%                     s = s + w([i, k], v, M) * state1(i - v + k);
%                 end
%                 state1(i) = (1 + s) / (1 - w0(i, v, M) - w([i, v], v, M));
%             else 
%                 state1(i) = 1;
%             end
%             
%             %lower bound
%             if i > v        
%                 %state2(i) = (1 + sum(w([zeros(v-1, 1) + i, (1:v-1)'], v) .* state2(i - v + (1:v-1)', v, M) )) ./ (1 - w0(i, v) - w([i, zeros(length(i), 1)+v], v));        
%                 s = 0;
%                 for k = 1:v
%                     s = s + w([i, k], v, M) * state2(i - v + k - 1);
%                 end
%                 state2(i) = (1 + s) / (1 - w0(i, v, M));
%             else 
%                 state2(i) = 1;
%             end
%             
%             result(j, :) = [M, state2(d * v), state1(d * v)];
%         end
%     end
%     
%     figure;
%     grid on
%     hold on
%     result = abs(result);
%     plot(result(:, 1), result(:, 2), '--');
%     plot(result(:, 1), result(:, 3));
%     hold off
%     title(sprintf('D = %d', d));
%     xlabel('average number of nodes in range')
%     ylabel('number of hops (avg � std)')
%     legend('lower bound', 'upper bound')
% end
% 
% 
