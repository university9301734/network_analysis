classdef SimpleQueue < handle
    %SIMPLEQUEUE Simulate queue with slotted time
    
    properties(Access = private)
        x % number of users in the system
        max_q % max queue length
        delays %list of delays per user
        tot_delay
        tot_users
        tot_user_dumped
        has_overflown
        story_x
        arr
        serv     
    end
    
    methods
        function this = SimpleQueue(arrivals, server, varargin)            
            this.arr = arrivals;
            this.serv = server;
            this.max_q = intmax('uint64'); %infinite queue
            if nargin == 3
                this.max_q = varargin{1};
            end
        end
        
        %execute a simulation of n_slots time slots
        function [system_story, avg_delay, p_overflow] = simulate(this, n_slots)
            this.x = 0;
            this.tot_delay = 0;
            this.tot_users = 0;
            this.story_x = zeros(n_slots, 1);
            this.has_overflown = 0;
            this.tot_user_dumped = 0;
            
            for i = 1:n_slots
                this.oneSlot(i);
            end
            
            system_story = this.story_x;
%             avg_delay = this.tot_delay / this.tot_users;
            avg_delay = sum(this.story_x) / this.tot_users; %this is an approximation of the delay
            p_overflow = this.tot_user_dumped / this.tot_users;
        end         
        
        function [bool] = hasOverflown(this)
            bool = this.has_overflown;
        end
       
    end
    
    methods(Access = private)
        %Simulate one time slot
        function oneSlot(this, idx)            
            if this.serv.isBusy()
                this.serv.update()
                if ~this.serv.isBusy()
                    this.x = this.x - 1;
%                     this.tot_delay = this.tot_delay + (idx - this.delays(1));
%                     this.delays(1) = [];
                end
            end
            
            if this.x > 0 && ~this.serv.isBusy()                
                this.serv.startService();
            end
%             this.x = this.x - this.serv.isBusy();
            arrivals = this.arr.getArrivals();
            if  this.x - 1 + arrivals >= this.max_q   % this.x - 1 == queue size
                tmp = this.max_q - (this.x - 1); %clamp arrivals to the max_q
                this.tot_user_dumped = this.tot_user_dumped + arrivals - tmp;
                arrivals = tmp;
                this.has_overflown = 1;                
            end
            this.x = this.x + arrivals;
            this.tot_users = this.tot_users + arrivals;
%             for i = 1:arrivals
%                 this.delays = [this.delays; idx]; %record time of arrival of last user(s)
%             end
            this.story_x(idx) = this.x;          
        end
    end
    
end

