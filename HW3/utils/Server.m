classdef Server < handle
    %SERVER Simulate a single server
    
    properties(Access = protected)
        busy_for_slots % how many slots the server will still be busy, including the current one.
    end 
    
    methods(Abstract)
        startService(this)
    end
    
    methods
        function res = isBusy(this)
            res = this.busy_for_slots > 0;
        end
        
        % to be overridded if necessary
        function update(this)
            if this.isBusy()
                this.busy_for_slots = this.busy_for_slots - 1;
            end
        end
    end
    
end

