classdef Arrivals < handle
    %ARRIVALS Generates arrivals
    
    properties
        pdf
    end
    
    methods
        %pdf_vec: vector describing the discrete pdf of the arrivals excluding the zero arrival probability.
        %pdf_vec(1) = P[1 arrival], pdf_vec(2) = P[2 arrivals] ... --> P[0
        %arrivals] = 1 - P[arrivals >= 1]
        function this = Arrivals(pdf_vec)
            try
                this.pdf = [ 1 - sum(pdf_vec), pdf_vec]; %row vec
            catch
                this.pdf = [ 1 - sum(pdf_vec); pdf_vec]; %col vec
            end
        end
        
        function arr = getArrivals(this)
%             [rows, cols] = size(this.pdf);
%             if rows >= cols
%                 arr = sum(rand(length(this.pdf), 1) <= this.pdf);
%             else
%                 arr = sum(rand(1, length(this.pdf)) <= this.pdf);
%             end
            
            x = rand(1,1);
            s = 0;
            i = 1;
            while s <= x
                s = s + this.pdf(i);
                i = i + 1;
            end
            
            arr = (i - 1) - 1; % the first -1 is to account for matlab crap indexing, the second -1 is for the while cicle exit condition
        end
    end
    
end

