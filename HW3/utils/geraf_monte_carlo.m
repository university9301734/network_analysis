%% This function is supposed to be launched in an 'arrayfun' for the GPU

function [n] = geraf_monte_carlo(M, d)
    n = 0; % # of hops
    reached = false;
    last_dist = d;        
    
    while (~reached)  
        %generate poisson rv
        x = exp(-M);
        prod = rand();
        N = 0;
        while prod > x
            prod = prod * rand();
            N = N + 1;
        end
    
        n = n + 1;
        if N == 0
            continue; %no luck, retry tx next turn               
        end

        advance = 0;
        while N > 0
            ad = sqrt(rand()) * cos(rand() * 2 * pi);
            if ad > advance
                advance = ad;
            end
            N = N - 1; 
        end


        last_dist = last_dist - advance;
        reached = last_dist < 0;
    end            
end