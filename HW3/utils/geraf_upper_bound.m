function [res] = geraf_upper_bound(M, d, v)
    BETA = @(x,y) 0.5 * (pi - 2 * asin(x./y) - 2 * x./y .* (1-(x./y).^2).^0.5);
    A = @(r, D) BETA((D.^2 - r.^2 + 1)./(2*D), 1) + BETA(D - (D.^2 - r.^2 + 1)./(2*D), r);
    w = @(ik, v, M) ( exp(- M * A((ik(:, 1)-v+ik(:, 2)-1)/v, ik(:, 1)/v) / pi) - exp(- M * A((ik(:, 1)-v+ik(:, 2))/v, ik(:, 1)/v) / pi) ) .* double(ik(:, 2) > 0 & ik(:, 2) <= v);
    w0 = @(i, v, M) exp(- M * A(i/v, i/v) / pi);

    state = zeros(d * v, 1);
    for i = 1:d*v
        %upper bound
        if i > v        
            s = 0;
            for k = 1:v-1
                s = s + w([i, k], v, M) * state(i - v + k);
            end
            state(i) = (1 + s) / (1 - w0(i, v, M) - w([i, v], v, M));
        else 
            state(i) = 1;
        end
    end
    res = state(d * v);
end