classdef ServerConstant < Server
    %SERVERCONSTANT Serve a user in a constant time
    
    properties(Access = private)
        s_time
    end
    
    methods
        function this = ServerConstant(service_time)
            this.s_time = service_time;
            this.busy_for_slots = 0;
        end
        
        function startService(this)
            this.busy_for_slots = this.s_time;
        end
        
        function update(this)
            update@Server(this);
        end
    end
    
end

