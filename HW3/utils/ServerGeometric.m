classdef ServerGeometric < Server
    %SERVERGEOMETRIC Serve a user in a geometric time (in slots)
    
     properties(Access = private)
        p
    end
    
    methods
        function this = ServerGeometric(mean)
            this.p = 1/mean;
            this.busy_for_slots = 0;
        end
        
        function startService(this)
            this.busy_for_slots = geornd(this.p) + 1;
        end
        
        function update(this)
            update@Server(this);
        end
    end
    
end

