%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ES 2 of HW 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
addpath(genpath('utils'))

sigma_dB = 8;
sigma = 0.1 * log(10) * sigma_dB;
sigma2 = sigma; %sigma^2;
rad_2pi = (2*pi)^0.5;
PT = 0.5; %transmitting power
CRF = [1, 3, 4, 7]; %cell reuse factor 
all_b = 10 .^ ([6, 10]./10); %outage threshold
K = 1;
nu = 4;

N = @(mu, sig2) mu + sig2 * randn(1,1); %Normal variable
PR = @(r) (raylrnd(1))^2 * exp(N(0, sigma2)) * (r^(-nu)); %  * K * PT; %Received power

%% Monte Carlo method

%Outage probability
rep = 1e4;
all_n = CRF;
all_a = 0.1:0.1:1;
all_Ps = cell(4, 1);
k_interf = 6;
tic
parfor j = 1:length(all_n)
    n = all_n(j); %due to the parfor
    PPP = zeros(length(all_a), length(all_b));
    for z = 1:length(all_a)
        a = all_a(z);
        Ps = zeros(1, length(all_b));
        
        %generate binomial
        Bin = rand(rep, k_interf);
        Bin = sum(Bin <= a, 2);
        
        for w = 1:length(all_b)           
            b = all_b(w);           
            for r = 1:rep %actual monte carlo             
                %k = binornd(k_interf, a, 1, 1);
                k = Bin(r);
                s = 0;
                R1 = (3*n)^0.5 - 1;
                R2 = (3*n)^0.5 + 1;
                for i = 1:k
                    s = s + PR(sqrt(rand(1,1)*(R2^2-R1^2)+R1^2)); 
                end
                t = PR(rand(1,1)^0.5);
                Ps(w) = Ps(w) + ( t > b * s );
            end
        end
        PPP(z, :) = Ps / rep;
    end
    all_Ps{j} = PPP;
end
toc

for w = 1:length(all_b)
    figure;
    grid on
    hold on
    for j = 1:length(all_n)    
        Ps_N = 1 - all_Ps{j};% outage prob
        plot(all_a, Ps_N(:, w)); 
        text(all_a(end-1), Ps_N(end-1, w)+0.02, sprintf('N = %d', all_n(j)));
    end
    hold off
    title(sprintf('b = %.1f dB', pow2db(all_b(w))));
    xlabel('interferers'' activity probability')
    ylabel('outage probability')
end

pause(0.1) % to let matlab show up the plots

%Capture probability
rep = 1e5;
all_k = 2:2:30;
tic
Cn = zeros(length(all_k), length(all_b));
parfor z = 1:length(all_k)
    k = all_k(z);
    Cn_tmp = zeros(1, length(all_b));
    for w = 1:length(all_b)           
        b = all_b(w);           
        for r = 1:rep %actual monte carlo                             
            interf = zeros(k, 1);
            h = k;
            while (h > 0)
                interf(h) = PR(sqrt(0.5*rand(1,1)));
                h = h - 1;
            end
            M = max(interf);
            if isempty(M)
                M = 0;
            end
            interf = sum(interf) - M;
            Cn_tmp(w) = Cn_tmp(w) + ( M > b * interf );
        end
    end
    Cn(z, :) = Cn_tmp / rep;
end
toc

figure;
hold on
for w = 1:length(all_b)
    grid on 
    plot(all_k, Cn(:, w)); 
    xlabel('collision size')
    ylabel('capture probability')
end
title('Capture probabilities')
legend('b = 6db', 'b = 10db');
hold off;

pause(0.1) % to let matlab show up the plots

%Slotted ALOHA
rep = 1e4;
all_a = 0:0.1:1; %transmit probability
users = 7;
tic
S = zeros(length(all_a), length(all_b));
parfor z = 1:length(all_a)
    a = all_a(z);
    Sn = zeros(1, length(all_b));     
    for w = 1:length(all_b)   
        Ps_n = 0;
        b = all_b(w);             
        for r = 1:rep %actual monte carlo                
            k = poissrnd(users * a, 1, 1);
            interf = zeros(k, 1);
            while (k > 0)
                interf(k) = PR(sqrt(rand(1,1)));
                k = k - 1;
            end
            M = max(interf);
            if isempty(M)
                M = 0;
            end
            interf = sum(interf) - M;
            Ps_n = Ps_n + ( M > b * interf ); %success prob                
        end          
        Sn(w) = Ps_n/rep;
    end
    S(z, :) = Sn;
end
toc 
figure;
grid on
hold on
for w = 1:length(all_b)
    plot(all_a * users, S(:, w)); 
    xlabel('G')
    ylabel('S')
end
title('Slotted ALOHA')
legend('b = 6db','b = 10db')
hold off