%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ES 1 of HW 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
addpath(genpath('utils'))

%% Es 1.a.i
all_a = 0.003:0.03:0.333;
rhos = zeros(length(all_a), 1);
delays = rhos;
var_delays = rhos;
rep = 100;
tic;
parfor i = 1:length(all_a)
    a = all_a(i);
    rhos(i) = 3 * a;
    q = SimpleQueue(Arrivals([a, a]), ServerConstant(1));
    delays_i = zeros(rep, 1);
    for j = 1:rep
        [q_state, q_delay] = q.simulate(10000);
        delays_i(j) = q_delay;
    end    
    delays(i) = mean(delays_i);
    var_delays(i) = var(delays_i);
end
disp('ES 1.a.i runtime:')
toc

figure
errors = 1.96 * (var_delays / rep ) .^ 0.5;
hold on
plot(rhos, delays);
errorbar(rhos, delays, errors, 'r.');
hold off
title('Es 1.a.i');
ylabel('delay');
xlabel('utilization factor \rho');

%% Es 1.a.ii
for a = [1/4, 1/3, 1/2]
    q = SimpleQueue(Arrivals([a, a]), ServerConstant(1));
    [q_state, q_delay] = q.simulate(10000);
    figure;
    plot(q_state);
    title(sprintf('ES 1.a.ii : a = %1.3f', a));
end

%% Es 1.b.i
all_b = 0.5:0.05:1;
rhos = zeros(length(all_b), 1);
delays = rhos;
var_delays = rhos;
rep = 100;
a = 0.5;
tic;
parfor i = 1:length(all_b)
    b = all_b(i);
    rhos(i) = a / b;
    q = SimpleQueue(Arrivals(a), ServerGeometric(1/b));
    delays_i = zeros(rep, 1);
    for j = 1:rep
        [q_state, q_delay] = q.simulate(10000);
        delays_i(j) = q_delay;
    end    
    delays(i) = mean(delays_i);
    var_delays(i) = var(delays_i);
end
disp('ES 1.b.i runtime:')
toc

figure
errors = 1.96 * (var_delays / rep ) .^ 0.5;
hold on
plot(rhos, delays);
errorbar(rhos, delays, errors, 'r.');
hold off
title('Es 1.b.i');
ylabel('delay');
xlabel('utilization factor \rho');

%% Es 1.b.ii
a = 0.5;
for b = [1/3, 1/2, 2/3]
    q = SimpleQueue(Arrivals(a), ServerGeometric(1/b));
    [q_state, q_delay] = q.simulate(10000);
    figure;
    plot(q_state);
    title(sprintf('ES 1.b.ii : b = %1.3f', b));
end


%% Es 1.c
p_overflow = 1e-5;
rep = 100;

% referring to Es 1.a.ii
all_a = [1/4];
q_sizes = zeros(rep, length(all_a));
parfor r = 1:rep
    q_sizes_tmp = zeros(1, length(all_a));
    for i = 1:length(all_a)
        p = 1;
        a = all_a(i);
        q_size = 50;
        last_p = 0;
        tries = 0;
        last_q_size = 0;

        while tries < 25

            q = SimpleQueue(Arrivals([a,a]), ServerConstant(1), q_size);
            [~, ~, p] = q.simulate(10000);
            %fprintf('%03d->%d) tries: %02d -> q_size: %d, p: %1.6f\n', r, i, tries, q_size, p);

            if  p < 1e-6 && abs(last_q_size - q_size) > last_q_size * 0.1 % p == 0 and we are far away from the optimal
                last_q_size = q_size;
                q_size = round(q_size / 2);
            else
                last_q_size = q_size;
                increment = round(q_size * q_size * (p-p_overflow));
                if increment == 0
                    increment = 1 * sign(p-p_overflow);
                end    
                q_size = q_size + increment;
            end        
            tries = tries + 1;           
        end
        q_sizes_tmp(i) = q_size;
    end
    q_sizes(r, :) = q_sizes_tmp;
end
fprintf('optimal queue sizes for Es 1.a.ii: \n\ta=1/4 : %2.3f \n\ta=1/3 : %2.3f \n\ta=1/2 : infinite \n', mean(q_sizes(:, 1)), mean(q_sizes(:, 2)));

% referring to Es 1.b.ii
all_b = [2/3];
q_sizes = zeros(rep, length(all_b));
parfor r = 1:rep
    q_sizes_tmp = zeros(1, length(all_b));
    for i = 1:length(all_b)
        p = 1;
        a = 0.5;
        b = all_b(i);
        q_size = 50;
        last_p = 0;
        tries = 0;
        last_q_size = 0;

        while tries < 25

            q = SimpleQueue(Arrivals(a), ServerGeometric(1/b), q_size);
            [~, ~, p] = q.simulate(10000);
            fprintf('%03d->%d) tries: %02d -> q_size: %d, p: %1.6f\n', r, i, tries, q_size, p);

            if  p < 1e-6 && abs(last_q_size - q_size) > last_q_size * 0.1 % p == 0 and we are far away from the optimal
                last_q_size = q_size;
                q_size = round(q_size / 2);
            else
                last_q_size = q_size;
                increment = round(q_size * q_size * (p-p_overflow));
                if increment == 0
                    increment = 1 * sign(p-p_overflow);
                end    
                q_size = q_size + increment;
            end        
            tries = tries + 1;           
        end
        q_sizes_tmp(i) = q_size;
    end
    q_sizes(r, :) = q_sizes_tmp;
end
fprintf('optimal queue sizes for Es 1.b.ii: \n\tb=1/3 : infinite \n\tb=1/2 : %2.3f  \n\tb=2/3 : %2.3f\n', mean(q_sizes(:, 1)), mean(q_sizes(:, 2)));

