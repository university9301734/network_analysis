%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ES 2 of HW 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
addpath(genpath('utils'))

sigma_dB = 8;
sigma = 0.1 * log(10) * sigma_dB;
sigma2 = sigma; %sigma^2;
rad_2pi = (2*pi)^0.5;
PT = 0.5; %transmitting power
CRF = [1, 3, 4, 7]; %cell reuse factor 
all_b = 10 .^ ([6, 10]./10); %outage threshold
K = 1;
nu = 4;

N = @(mu, sig2) mu + sig2 * randn(1,1); %Normal variable
Npdf = @(val, sig) (1 / (rad_2pi * sig)) * exp(-(val.^2) / (2 * sig * sig));  
h = @(r, R1, R2) 2 * r / (R2*R2 - R1*R1); %pdf of the radius
Idr = @(xi0, r0, xi, r, R1, R2, b) h(r, R1, R2) / (1 + b * exp(xi-xi0) * (r / r0)^(-nu)); %most inner integrand function

%weights W and sampling points X
n_points = 12;
X = [0.1252334085114689, 0.3678314989981802, 0.5873179542866175, 0.7699026741943047, 0.9041172563704749, 0.9815606342467192];
X = [-X, X];
W = [0.2491470458134028, 0.2334925365383548, 0.2031674267230659, 0.1600783285433462, 0.1069393259953184, 0.0471753363865118];
W = [W, W];



%% Gaussian quadrature rule

%Outage probability
pi_sig  = (2 * pi * sigma2)^0.5;
all_a = 0.1:0.1:1;
Ps = cell(length(CRF), 1);
infty = 10;
for i = 1:length(CRF)
    n = CRF(i);
    R1 = (3*n)^0.5-1;
    R2 = (3*n)^0.5+1;
    Ps_tmp = zeros(length(all_a), length(all_b));
    for j = 1:length(all_b)
        b = all_b(j);
        for z = 1:length(all_a)
            a = all_a(z);            
            for r = 1:length(X)
                r0 = X(r);
                %change variables
                y0 = r0/2 + 1/2;
                dy0 = 1/2;
                
                Ps_r0 = 0;
                for e = 1:length(X)
                    xi0 = X(e);
                    %change variables
                    chi0 = infty * xi0;
                    dchi0 = infty;               
                    
                    I = 0;
                    for w = 1:length(X)
                        xi = X(w);
                        %change variables
                        chi = infty * xi;
                        dchi = infty;
                        
                        I2 = 0;
                        for q = 1:length(X)
                            ri = X(q);
                            %change variables
                            yi = (R2-R1)/2 * ri + (R2+R1)/2;
                            dyi = (R2-R1)/2;
                            
                            I2 = I2 + W(q) * Idr(chi0, y0, chi, yi, R1, R2, b) * dyi;
                        end
                        
                        I = I + W(w) * I2 * Npdf(chi, sigma) * dchi;  
                    end    
                    
                    Ps_r0 = Ps_r0 + W(e) * (1 - a + a * I)^6 * Npdf(chi0, sigma) * dchi0;
                end        
                Ps_tmp(z, j) = Ps_tmp(z, j) + W(r) * Ps_r0 * h(y0, 0, 1) * dy0;
            end
        end
    end
    Ps{i} = Ps_tmp;
end


for j = 1:length(all_b)
    figure;
    hold on
    grid on
    for i = 1:length(CRF)
        P = Ps{i};
        plot(all_a, 1 - P(:, j));
        text(all_a(end-1), 1 - P(end-1, j)+0.02, sprintf('N = %d', CRF(i)));
    end
    title(sprintf('b = %.1f db', pow2db(all_b(j))));
    xlabel('interferers'' activity probability')
    ylabel('outage probability')
    hold off
end

%capture probability
all_k = 2:30;
Ps = cell(length(CRF), 1);
infty = 10;

R1 = 0;
R2 = 1;
Cn = zeros(length(all_k), length(all_b));
for j = 1:length(all_b)
    b = all_b(j);
    for z = 1:length(all_k)
        k = all_k(z);            
        for r = 1:length(X)
            r0 = X(r);
            %change variables
            y0 = r0/2 + 1/2;
            dy0 = 1/2;

            Ps_r0 = 0;
            for e = 1:length(X)
                xi0 = X(e);
                %change variables
                chi0 = infty * xi0;
                dchi0 = infty;               

                I = 0;
                for w = 1:length(X)
                    xi = X(w);
                    %change variables
                    chi = infty * xi;
                    dchi = infty;

                    I2 = 0;
                    for q = 1:length(X)
                        ri = X(q);
                        %change variables
                        yi = (R2-R1)/2 * ri + (R2+R1)/2;
                        dyi = (R2-R1)/2;

                        I2 = I2 + W(q) * Idr(chi0, y0, chi, yi, R1, R2, b) * dyi;
                    end

                    I = I + W(w) * I2 * Npdf(chi, sigma) * dchi;  
                end    

                Ps_r0 = Ps_r0 + W(e) * k * I^(k-1) * Npdf(chi0, sigma) * dchi0;
            end        
            Cn(z, j) = Cn(z, j) + W(r) * Ps_r0 * h(y0, 0, 1) * dy0;
        end
    end
end

figure;
hold on
grid on
for j = 1:length(all_b)
    plot(all_k, Cn(:, j));
   
    xlabel('collision size')
    ylabel('capture probability')
end
title('Capture probabilities')
legend('b = 6db', 'b = 10db');
hold off

%slotted ALOHA
users = 7;
all_k = 0:0.05:1; %k are now probabilities
Ps = cell(length(CRF), 1);
infty = 10;

R1 = 0;
R2 = 1;
Cn = zeros(length(all_k), length(all_b));
for j = 1:length(all_b)
    b = all_b(j);
    for z = 1:length(all_k)
        k = all_k(z);            
        for r = 1:length(X)
            r0 = X(r);
            %change variables
            y0 = r0/2 + 1/2;
            dy0 = 1/2;

            Ps_r0 = 0;
            for e = 1:length(X)
                xi0 = X(e);
                %change variables
                chi0 = infty * xi0;
                dchi0 = infty;               

                I = 0;
                for w = 1:length(X)
                    xi = X(w);
                    %change variables
                    chi = infty * xi;
                    dchi = infty;

                    I2 = 0;
                    for q = 1:length(X)
                        ri = X(q);
                        %change variables
                        yi = (R2-R1)/2 * ri + (R2+R1)/2;
                        dyi = (R2-R1)/2;

                        I2 = I2 + W(q) * Idr(chi0, y0, chi, yi, R1, R2, b) * dyi;
                    end

                    I = I + W(w) * I2 * Npdf(chi, sigma) * dchi;  
                end    
                G = k * users;
                Ps_r0 = Ps_r0 + W(e) * G * exp(-G * (1 - I)) * Npdf(chi0, sigma) * dchi0;
            end        
            Cn(z, j) = Cn(z, j) + W(r) * Ps_r0 * h(y0, 0, 1) * dy0;
        end
    end
end

figure;
hold on
grid on
for j = 1:length(all_b)
    plot(all_k, Cn(:, j));
   
    xlabel('G')
    ylabel('S')
end
title('Slotted ALOHA')
legend('b = 6db', 'b = 10db');
hold off