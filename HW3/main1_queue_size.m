clear all

a = 1/4;
H = [1-2*a, a, a];
L = length(H);

for Q = 4:400
    P = zeros(Q, Q);
    P(1, 1:L) = H;
    P(Q, Q-1:end) = [H(1), 1-H(1)];
    for k = 2:Q-1
        P(k, (k-1):(k-2+L)) = H;
    end
    P = P^500; %find stationary distribution
    if H(2) * P(1, Q) < 10^-5
        break
    end
end

fprintf('1) Q >= %d\n', Q);

a = 1/2;
b = (2/3);
H = [(1-a)*b, a*b+(1-a)*(1-b), a*(1-b)];
for Q = 4:400
    P = zeros(Q,Q);
    P(1, 1:2) = [1-H(2), H(2)];
    P(Q, Q-1:end) = [H(1), 1-H(1)];
    for k = 2:Q-1
        P(k, (k-1):(k-2+L)) = H;
    end

   P = P^500;
   if H(2) * P(1, Q) < 10^-5
        break
   end
end
fprintf('2) Q >= %d\n', Q);