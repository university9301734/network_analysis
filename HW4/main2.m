%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Es 2 HW4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

lambda = [4, 12];
mu = 1;
rep = 1000;
p_good = 0.5;

%% Raw estimator
res = [];
for n = 1:rep
    i = 1 + (rand(1,1) > p_good); % select good day or not
    
    q = Queue(PoissonProcess(lambda(i)), PoissonProcess(mu));
    [X, Q] = q.simulate('MAX_TIME', 10);
    
    res(end + 1) = Q(end); % user left in the queue at shutdown
end

CI_raw = 1.96 * std(res) / rep^0.5;
raw = mean(res);

fprintf('Raw estimation and CI at 95%% after 1000 iterations: %3.2f +/- %3.2f\n', raw, CI_raw)

% res = [];
% n = 0;
% CI_raw = 1e3;
% while CI_raw > 3 || n < 10
%     i = 1 + (rand(1,1) > p_good); % select good day or not
%     
%     q = Queue(PoissonProcess(lambda(i)), PoissonProcess(mu));
%     [X, Q] = q.simulate('MAX_TIME', 10);
%     
%     res(end + 1) = Q(end); % user left in the queue at shutdown
%     n = n + 1;
%     CI_raw = 1.96 * std(res) / n^0.5;
% end
% 
% CI_raw = 1.96 * std(res) / rep^0.5;
% raw = mean(res);
% fprintf('Nunber of iterations to reach a CI of 3 at 95%% and raw estimation: %d, %3.2f \n', raw, n)

%% Stratified estimator (see pag 166 onwards)

res1 = [];
res2 = [];
rep1 = rep * p_good;
rep2 = rep * (1 - p_good);

for n1 = 1:rep1
     q = Queue(PoissonProcess(lambda(1)), PoissonProcess(mu));
    [X, Q] = q.simulate('MAX_TIME', 10);
    
    res1(end + 1) = Q(end); % user left in the queue at shutdown
end

for n2 = 1:rep2
     q = Queue(PoissonProcess(lambda(2)), PoissonProcess(mu));
    [X, Q] = q.simulate('MAX_TIME', 10);
    
    res2(end + 1) = Q(end); % user left in the queue at shutdown
end

m1 = mean(res1);
m2 = mean(res2);
var_eps = (var(res1) + var(res2)) / rep;
CI_eps = 1.96 * var_eps^0.5 / rep^0.5;
eps = m1 * p_good + m2 * (1 - p_good);

fprintf('Statified estimation and CI at 95%% after 1000 iterations: %3.2f +/- %3.2f\n', eps, CI_eps)