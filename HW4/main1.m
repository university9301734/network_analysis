%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Es 1 HW4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all

addpath(genpath('utils'));


all_b = 0.5:0.05:1;
a = 1/2;
rhos = zeros(length(all_b), 1);
delays = rhos;
var_delays = rhos;
rep = 100;
tic;
parfor i = 1:length(all_b)
    b = all_b(i);
    rhos(i) = a / b ;
    q = Queue(ConstantTimeProcess([1 - a, a]), ConstantTimeProcess([1 - b, b]));
    delays_i = zeros(rep, 1);
    for j = 1:rep
        q.reset_system();
        [~, ~, ~, ~, ~, ~, avg_delay] = q.simulate('MAX_TIME', 10000);
        delays_i(j) = avg_delay;
    end    
    delays(i) = mean(delays_i);
    var_delays(i) = var(delays_i);
end
disp('ES 1.a.i runtime:')
toc

figure
errors = 1.96 * (var_delays / rep ) .^ 0.5;
hold on
plot(rhos, delays);
errorbar(rhos, delays, errors, 'r.');
hold off
title('Es 1.a.i : \rho vs delay');
ylabel('delay');
xlabel('utilization factor \rho');

figure
% axis manual
% L = length(T);
% axis([0 L 0 L]);
b = 1/3;
q = Queue(ConstantTimeProcess([1 - a, a]), ConstantTimeProcess([1 - b, b]));
[X, ~, ~, T, ~, ~, ~] = q.simulate('MAX_TIME', 10000);
plot(T, X);
title('Es 1.a.ii : System state, b = 1/3')
xlabel('Time')
ylabel('User(s) in the system')

figure
% axis manual
% L = length(T);
% axis([0 L 0 L]);
b = 1/2;
q = Queue(ConstantTimeProcess([1 - a, a]), ConstantTimeProcess([1 - b, b]));
[X, ~, ~, T, ~, ~, ~] = q.simulate('MAX_TIME', 10000);
plot(T, X);
title('Es 1.a.ii : System state, b = 1/2')
xlabel('Time')
ylabel('User(s) in the system')

figure
% axis manual
% L = length(T);
% axis([0 L 0 L]);
b = 2/3;
q = Queue(ConstantTimeProcess([1 - a, a]), ConstantTimeProcess([1 - b, b]));
[X, ~, ~, T, ~, ~, ~] = q.simulate('MAX_TIME', 10000);
plot(T, X);
title('Es 1.a.ii : System state, b = 2/3')
xlabel('Time')
ylabel('User(s) in the system')



b = 1/2;
all_l = 0.25:0.05:(1/(3*b));
all_l(end+1) = 1/(3*b);
rhos = zeros(length(all_l), 1);
delays = rhos;
var_delays = rhos;
rep = 100;
tic;
parfor i = 1:length(all_l)
    l = all_l(i);
    rhos(i) = l / (1/(3*b));
    q = Queue(PoissonProcess(l), ConstantTimeServiceProcess([b, 1 - b]));
    delays_i = zeros(rep, 1);
    for j = 1:rep
        q.reset_system();
        [~, ~, ~, ~, ~, ~, avg_delay] = q.simulate('MAX_TIME', 1000);
        delays_i(j) = avg_delay;
    end    
    delays(i) = mean(delays_i);
    var_delays(i) = var(delays_i);
end
disp('ES 1.b runtime:')
toc

figure
errors = 1.96 * (var_delays / rep ) .^ 0.5;
hold on
plot(rhos, delays);
errorbar(rhos, delays, errors, 'r.');
hold off
title('Es 1.b : \rho vs delay');
ylabel('delay');
xlabel('utilization factor \rho');