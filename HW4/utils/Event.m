classdef Event < handle
    properties
       num_users
       time
    end
    
    methods
        function this = Event(t, n)
           this.time = t;
           this.num_users = n;
        end
        
        function setTime(this, t)
            this.time = t;
        end
        
        function t = getTime(this)
            t = this.time;
        end
       
        function setUsers(this, n)
           this.num_users = n; 
        end
        
        function n = getUsers(this)
           n = this.num_users; 
        end
    end
end