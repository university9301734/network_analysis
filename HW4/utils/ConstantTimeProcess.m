
%% Generate (multiple) arrivals in one time slot. The property time returned by getTime() is actually how much time is passed since the last arrival, not the length of such interval

classdef ConstantTimeProcess < AbstractProcess
    properties
       pdf 
       time
    end
    
    
    methods
        % pdf must specify prob of 0 arrivals, 1 arrival, 2 arrivals, 
        % 3 arrivals ... (>= 1 arrivals are optional)
        % (optional) the length of one time interval can be also specified
        % as second parameters
        function this = ConstantTimeProcess(pdf, varargin)
            this.pdf = pdf;
            this.time = 1;
            if nargin > 1
                this.time = varargin{1};
            end
        end
        
        function event = getNextEvent(this)
            u = rand(1,1) * (1 - this.pdf(1));
            s = 0;
            n = 1;
            while (true)
                s = s + this.pdf(n+1);
                if s > u
                    break
                end
                n = n + 1;
            end
            
            event = Event((geornd(1 - this.pdf(1)) + 1) * this.time, n);
        end
    end
end