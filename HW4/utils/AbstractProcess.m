classdef (Abstract) AbstractProcess < handle
   
    methods
        event = getNextEvent(this);
    end
end