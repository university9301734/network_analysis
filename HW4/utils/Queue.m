classdef Queue < handle
    properties             
        % System state
        server_busy
        users_queued
        times_of_arrival_list
        last_event_time
        
        % counters
        clock
        A
        D
        
        % statistics
        total_delay
        served_users
        Q_list
        area_Q
        B_list
        area_B
        X_list
        Time_list
        
        % others
        arr_proc
        arr_evt_list
        serv_proc
        dept_evt_list
        
    end
    
    methods
        function [this] = Queue(arr_proc, serv_proc)
            this.reset_system();
            
            if ~isa(arr_proc, 'AbstractProcess') || ~isa(serv_proc, 'AbstractProcess')
                E = MException('QUEUE:badArgument', 'arrival or departure process does not derive from class ''AbstractProcess'' ');
                throw(E);
            end
            
            this.arr_proc = arr_proc;
            this.serv_proc = serv_proc;
            
        end                
        
        function reset_system(this)
            this.server_busy = false;
            this.users_queued = 0;
            this.times_of_arrival_list = [];
            this.last_event_time = 0;
            
            this.clock = 0;
            this.A = 0;
            this.D = 1e12;
            
            this.total_delay = 0;
            this.served_users = 0;
            this.B_list = [];
            this.area_B = 0;
            this.Q_list = [];
            this.area_Q = 0;
            this.X_list = [];
            this.Time_list = [];
            
            this.arr_evt_list = cell(0);
            this.dept_evt_list = cell(0);
        end
        
        %returns System state (X), Queue state (Q), Server state (S), Time hystory (T), Area
        %under Q (A_Q), Area under S (A_S), average delay of the users.
        function [X, Q, S, T, A_Q, A_S, avg_delay] = simulate(this, stop_cond_name, stop_cond_value)      
            arr_evt = this.arr_proc.getNextEvent();
            dept_evt = this.serv_proc.getNextEvent();
            dept_evt.setTime(arr_evt.getTime() + dept_evt.getTime());
            this.arr_evt_list{end+1} = arr_evt;
            this.dept_evt_list{end+1} = dept_evt;
            
            
            func = 0;
            switch stop_cond_name
                case 'N_USERS'
                    func = @this.max_user;
                case 'MAX_TIME'
                    func = @this.max_time;
            end
            
            %main routine
            while ~func(stop_cond_value)
                if isempty(this.arr_evt_list)
                    this.departure_evt_manager();
                elseif isempty(this.dept_evt_list)
                    this.arrival_evt_manager();
                elseif this.arr_evt_list{1}.getTime() < this.dept_evt_list{1}.getTime()
                    this.arrival_evt_manager(); 
                else
                    this.departure_evt_manager();
                end
            end
            
            X = this.Q_list + this.B_list;
            Q = this.Q_list;
            S = this.B_list;
            T = this.Time_list;
            
            last_t = 0;
            A_Q = 0;
            A_S = 0;
            for i = 1:length(this.Q_list)
                inter_t = this.Time_list(i) - last_t;
                last_t = this.Time_list(i);
                A_Q = A_Q + this.Q_list(i) * inter_t;
                A_S = A_S + this.B_list(i) * inter_t;
            end
            
            avg_delay = this.total_delay / this.served_users;
        end
    end  
    
    methods (Access = private)
        % Stop condition funtion
        function [bool] = max_user(this, m)
           bool = this.served_users >= m; 
        end
        
        function [bool] = max_time(this, t)
            bool = this.clock >= t;
        end

        function arrival_evt_manager(this)
            event = this.arr_evt_list{1};
            this.arr_evt_list(1) = [];
            n = event.getUsers();
            t_arr = event.getTime();
            if t_arr < this.clock
               hello = 0; 
            end
            this.clock = t_arr;
            
            % schedule next arrival
            arr_evt = this.arr_proc.getNextEvent();
            arr_evt.setTime(arr_evt.getTime() + this.clock); %transform event's time from relative to global
            i = 1;
            L = length(this.arr_evt_list);
            while i < L && arr_evt.getTime() > this.arr_evt_list{i}  % keep list sorted by time
                i = i + 1;
            end
            this.arr_evt_list{i} = arr_evt;
            
            % manage current arrival
            if this.server_busy == true
                this.users_queued = this.users_queued + n;
                for i = 1:n
                    this.times_of_arrival_list(end+1) = t_arr;
                end                               
            else
                if n > 1
                    this.users_queued = this.users_queued + n - 1;
                    for i = 1:(n-1)
                        this.times_of_arrival_list(end+1) = t_arr;
                    end
                end
                this.served_users = this.served_users + 1;                    

                this.server_busy = true;
                
                %Schedule next departure
                dept_evt = this.serv_proc.getNextEvent();
                dept_evt.setTime(dept_evt.getTime() + this.clock); %transform event's time from relative to global
                i = 1;
                L = length(this.dept_evt_list);
                while i < L && dept_evt.getTime() > this.dept_evt_list{i} % keep list sorted by time
                    i = i + 1;
                end
                this.dept_evt_list{i} = dept_evt;
                
            end
            
            this.Q_list(end+1) = this.users_queued;
            this.B_list(end+1) = this.server_busy;
%             this.X_list(end+1) = this.users_queued + this.server_busy();
            this.Time_list(end+1) = this.clock;
        end
        
        function departure_evt_manager(this)
            event = this.dept_evt_list{1};
            this.dept_evt_list(1) = [];            
            t_dept = event.getTime();    
            if t_dept < this.clock
               hello = 0; 
            end
            this.clock = t_dept;
            
            if this.users_queued == 0
                this.server_busy = false;                
            else
                this.users_queued = this.users_queued - 1;
                this.total_delay = this.total_delay + (t_dept - this.times_of_arrival_list(1));
                this.times_of_arrival_list(1) = [];
                this.served_users = this.served_users + 1;
                
                % Schedule next departure
                dept_evt = this.serv_proc.getNextEvent();
                dept_evt.setTime(dept_evt.getTime() + this.clock); %transform event's time from relative to global
                i = 1;
                L = length(this.dept_evt_list);
                while i < L &&  dept_evt.getTime() > this.dept_evt_list{i} % keep list sorted by time
                    i = i + 1;
                end
                this.dept_evt_list{i} = dept_evt;
                
            end
            
            this.Q_list(end+1) = this.users_queued;
            this.B_list(end+1) = this.server_busy;
%             this.X_list(end+1) = this.users_queued + this.server_busy();
            this.Time_list(end+1) = this.clock;
        end
    end
end