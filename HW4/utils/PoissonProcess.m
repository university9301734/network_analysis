
%% Generate exponential inter times with rate lambda. The resulting point process is Poisson with paramenter lambda.

classdef PoissonProcess < AbstractProcess
    properties
       l 
    end
    
    methods
        function this = PoissonProcess(lambda)
            this.l = lambda;
        end
        
        function event = getNextEvent(this)
            t = - (1/this.l) * log(rand(1,1));
            event = Event(t, 1);
        end
    end 
end