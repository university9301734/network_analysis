
%% Generate (multiple) time slots to serve a single user. The property time returned by getTime() is actually how much time is passed since the last departure, not the length of such slots.

classdef ConstantTimeServiceProcess < AbstractProcess
    properties
       pdf 
       time
    end
    
    
    methods
        % pdf must specify prob of 1 slot, 2 slots, 
        % 3 slots ... (>= 1 slots are optional)
        %(OPTIONAL) Specify the time slot duration
        function this = ConstantTimeServiceProcess(pdf, varargin)
            this.pdf = pdf;
            this.time = 1;
            if nargin > 1
                this.time = varargin{1};
            end
        end
        
        function event = getNextEvent(this)
            u = rand(1,1);
            s = 0;
            t = 1;
            while (true)
                s = s + this.pdf(t);
                if s > u
                    break
                end
                t = t + 1;
            end
            
            event = Event(t * this.time, 1);
        end
    end
end