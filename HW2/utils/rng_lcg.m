%% Linear congruential random generator
% x, y: return matrix' dimension (row, cols)
% seed: starting seed of the rng
% (optional) a: parameter of the lcg
% (optional) m: prime number for the lcg
function [seq] = rng_lcg(x, y, seed, varargin)
    seq = zeros(x,y);
    m = 2^31 - 1;
    a = 16807;
    
    if nargin > 3
        a = varargin{1};
    end
    if nargin > 4
        m = varargin{2};
    end
    
    h = 1;
    s = seed;
    for i = 1:x
        for j = 1:y
            s = mod( s * (a ^ h), m);
            seq(i, j) = s;
        end
    end
    seq = seq / m;
end