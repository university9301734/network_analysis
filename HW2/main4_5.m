% Es 4) and 5) : Find some parameters about two Linear Congruential Random Generators
clear all
close all
addpath(genpath('utils'))

%% ES 4
lcg1 = rng_lcg(1000, 1, 1241513, 18, 101);
lcg2 = rng_lcg(1000, 1, 1241513,  2, 101);

period1 = unique(lcg1);
period2 = unique(lcg1);
if length(period1) == 100
    fprintf('lcg1 has full period\n')
else
    fprintf('lcg1 doesn''t have full period\n')
end

if length(period2) == 100
    fprintf('lcg2 has full period\n')
else
    fprintf('lcg2 doesn''t have full period\n')
end

figure;
x = lcg1(1:end-1);
y = lcg1(2:end);
plot(x, y, '.');
title('LCG 1')

figure;
x = lcg2(1:end-1);
y = lcg2(2:end);
plot(x, y, '.');
title('LCG 2')

%% ES 5
lcg3 = rng_lcg(1000, 1, 1241513,  65539, 2^31);

figure;
x = lcg3(1:end-1);
y = lcg3(2:end);
plot(x, y, '.');
title('LCG 3')

figure
x = lcg3(1:end-2);
y = lcg3(2:end-1);
z = lcg3(3:end);
hold on
plot3(x, y, z, '.');
title('LCG 3 - 3D')
