% 1) Reproduce figures 6.5, 6.7 and 6.10
addpath(genpath('utils'))
close all;
clear all;

%% Figure 6.5
seq = rng_lcg(1000, 1, 1);

corr = autocorr(seq, 29);

figure
hold on
bar(1:length(corr), corr,  0.05);
bound = 1.96 * 1/length(seq)^0.5;
plot([0, length(corr)], [bound, bound], 'b--');
plot([0, length(corr)], [-bound, -bound], 'b--');
title('Autocorrelation');
hold off

figure;
hold on
for i = 1:9
    subplot(3,3,i);
    j = i * 100;
    y = [seq((j+1):end); seq(1:j)]; %circle around the array
    plot(seq, y, '.');
    title(sprintf('Lag plot, h = %d', j));
end
hold off;

figure;
seq = sort(seq);
plot(seq); % QQplot for uniform distribution is just the order statistic
title('Uniform QQplot');


%% Figure 6.7
s1 = 1;
seq1 = rng_lcg(1000, 1, s1);
s2 = 2;
seq2 = rng_lcg(1000, 1, s2);
s3 = seq1(end);
seq3 = rng_lcg(1000, 1, s3);


figure;
plot(seq1, seq2, '.');
title('seeds s1 = 1 and s2 = 2');

figure;
plot(seq1, seq3, '.');
title('seeds s1 = 1 and s2 = seq1[end]');

%% Figure 6.10

sample1 = zeros(2000, 1);
for i = 1:2000
    U = rand(1,1);
    x = rand(1,1) * 20 - 10; % x ~ U[-10, 10]
    while (U >= sin(x)^2 / x^2)
        x = rand(1,1) * 20 - 10; % x ~ U[-10, 10]
        if abs(x) < 1e-9
            x = 1e-9 * sign(x);
        end
        U = rand(1,1);
    end
    sample1(i, :) = x;
end

sample2 = zeros(2000, 2);
for i = 1:2000
    x = rand(1,2);      
    U = rand(1,1);
    while U >= abs(x(1) - x(2))
        x = rand(1,2);      
        U = rand(1,1);
    end
    sample2(i, :) = x;
end

figure;
histogram(sample1, 'BinWidth', 0.1);

figure;
plot(sample2(:, 1), sample2(:, 2), '.');

