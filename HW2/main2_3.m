% Es 2) and 3) : Generate some distributions with different methods
addpath(genpath('utils'))
close all;
clear all;

n = 100;
p = 0.05;
n_rvs = 1000;
rep = 10000;

%% Es 2

figure;
% CDF inversion
bin_time1 = zeros(rep,1);
for j = 1:rep
    tic
    bin_seq1 = zeros(n_rvs, 1);
    num = factorial(n);
    saves = zeros(n,1);
    for i = 1:n_rvs
        k = -1;
        u = rand(1,1);
        tot = 0;
        last = (1-p)^n;
        while(k <= n && u >= tot)
            k = k + 1;
            if saves(k+1) == 0 %k+1 due to Matlab crap indexing
                saves(k+1) = factorial(k) * factorial(n-k);        
            end
            den = saves(k+1);
            tmp = last * p / (1-p);
            tot = tot + (num / den) *last;%* p^k * (1-p)^(n-k);
            last = tmp;
        end

        bin_seq1(i) = k;
    end
    bin_time1(j) = toc;
end
subplot(1,3,1)
histogram(bin_seq1, 'Normalization', 'pdf');
title('BIN - CDF inversion')

% Bernoulli's rvs
bin_time2 = zeros(rep, 1);
for j = 1:rep
    tic
    bin_seq2 = rand(n_rvs, n);
    bin_seq2 = bin_seq2 <= p;
    bin_seq2 = sum(bin_seq2, 2); %sum over rows
    bin_time2(j) = toc;
end

subplot(1,3,2)
histogram(bin_seq2, 'Normalization', 'pdf');
title('BIN - Bernoulli sum')

% Geometric sum
bin_time3 = zeros(rep, 1);
for j = 1:rep
    tic
    bin_seq3 = zeros(n_rvs, 1);
    acc = bin_seq3;
    filter = 1;
    while (sum(filter) > 0)
        acc = acc + (geornd(p, n_rvs, 1) + 1); % geornd returns the number of failures before the first success
        filter = acc <= n;
        bin_seq3(filter) = bin_seq3(filter) + 1;
    end
    bin_time3(j) = toc;
end
subplot(1,3,3)
histogram(bin_seq3, 'Normalization', 'pdf');
title('BIN - Geometric sum')


%% Es 3

figure;
l = n * p;

% CDF inversion
p_time1 = zeros(rep, 1);
for j = 1:rep
    tic
    p_seq1 = zeros(n_rvs, 1);
    saves = zeros(n,1);
    for i = 1:n_rvs
        k = -1;
        u = rand(1,1);
        tot = 0;
        last = 1;
        while(u >= tot)
            k = k + 1;
            if saves(k+1) == 0  %k+1 due to Matlab crap indexing
                saves(k+1) = factorial(k);
            end
            tot = tot + exp(-l) * last / saves(k+1);
            last = last * l; %l^k
        end

        p_seq1(i) = k;
    end
    p_time1(j) = toc;
end

subplot(1,3,1)
histogram(p_seq1, 'Normalization', 'pdf');
title('POIS - CDF inv');

% EXP sum
p_time2 = zeros(rep, 1);
for j = 1:rep
    tic
    p_seq2 = zeros(n_rvs, 1); 
    acc = p_seq2;
    filter = 1;
    while (sum(filter) > 0)
        acc = acc + exprnd(1/l, n_rvs, 1);
        filter = acc <= 1;
        p_seq2(filter) = p_seq2(filter) + 1;
    end
    p_time2(j) = toc;
end

subplot(1,3,2)
histogram(p_seq2, 'Normalization', 'pdf');
title('POIS - Exp sum');

% UNIF product
p_time3 = zeros(rep, 1);
for j = 1:rep
    tic
    p_seq3 = zeros(n_rvs, 1); 
    acc = ones(n_rvs, 1);
    filter = 1;
    a = exp(-l);
    while (sum(filter) > 0)
        acc = acc .* rand(n_rvs, 1);
        filter = acc >= a;
        p_seq3(filter) = p_seq3(filter) + 1;
    end
    p_time3(j) = toc;
end

subplot(1,3,3)
histogram(p_seq3, 'Normalization', 'pdf');
title('POIS - Unif product');


% test execution times of matlab
figure;
subplot(1,2,1)
bin_time4 = zeros(rep, 1);
for j = 1:rep
    tic
    bin_seq4 = binornd(n,p, n_rvs, 1);
    bin_time4(j) = toc;
end
histogram(bin_seq4, 'Normalization', 'pdf');
title('BIN - MATLAB');

subplot(1,2,2)
p_time4 = zeros(rep, 1);
for j = 1:rep
    tic
    p_seq4 = poissrnd(l, n_rvs, 1);
    p_time4(j) = toc;
end
histogram(p_seq4, 'Normalization', 'pdf');
title('POIS - MATLAB');


%% Find mean time to generate n_rvs variates
% BIN
fprintf('MEAN TIME TO GENERATE %d VARIATES (95%% Conf. Interv.)\n\n', n_rvs);
fprintf('Binomial:\n');
fprintf('\t1 - CDF inv: %1.6f ms +- %f \n', mean(bin_time1*1000) , 1.96 * (var(bin_time1*1000)/n_rvs)^0.5);
fprintf('\t2 - Bernoulli sum: %1.6f ms +- %f \n', mean(bin_time2*1000), 1.96 * (var(bin_time2*1000)/n_rvs)^0.5);
fprintf('\t3 - Geometric sum: %1.6f ms +- %f \n', mean(bin_time3*1000), 1.96 * (var(bin_time3*1000)/n_rvs)^0.5);
fprintf('\tReference (MATLAB): %1.6f ms +- %f \n', mean(bin_time4*1000), 1.96 * (var(bin_time4*1000)/n_rvs)^0.5);
% POISS
fprintf('Poisson:\n');
fprintf('\t1 - CDF inv: %1.6f ms +- %f \n', mean(p_time1*1000) , 1.96 * (var(p_time1*1000)/n_rvs)^0.5);
fprintf('\t2 - Exponential sum: %1.6f ms +- %f \n', mean(p_time2*1000), 1.96 * (var(p_time2*1000)/n_rvs)^0.5);
fprintf('\t3 - Uniform product: %1.6f ms +- %f \n', mean(p_time3*1000), 1.96 * (var(p_time3*1000)/n_rvs)^0.5);
fprintf('\tReference (MATLAB): %1.6f ms +- %f \n', mean(p_time4*1000), 1.96 * (var(p_time4*1000)/n_rvs)^0.5);

